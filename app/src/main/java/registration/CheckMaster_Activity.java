package registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import dlr.DLR_Registeration_Activity;
import jodidar.Jodidar_Registration_Activity;

import login.Login_Activity;
import msrso.MSR_LISSO_Resgistration_Activity;

import enduser.EndUser_Activity;
import com.brainmagic.lucasindianservice.demo.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.ArrayList;
import java.util.List;

import alertbox.Alertbox;
import api.models.checkmaster.Master;
import api.models.checkmaster.Master_data;
import api.retrofit.APIService;
import api.retrofit.RetroClient;
import network.NetworkConnection;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toast.StyleableToasty;

/**
 * A login screen that offers login via email/password.
 */
public class CheckMaster_Activity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {


    // UI references.
    private EditText mMobileNo;
    private MaterialSpinner mUsertype;
    private Button mLogin;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ArrayList<String> usertypelist;
    private Alertbox alertbox = new Alertbox(this);
    private StyleableToasty toasty = new StyleableToasty(this);
    private String mSelectedUsertype="Select";
    private ProgressDialog loading;
    private final static int PERMISSION_REQUEST_CODE = 121;
    private String mIMEI,mModelName,mModelNumber;
    private static final String[] PHONESTATE_AND_SMS =
            {Manifest.permission.READ_PHONE_STATE
//                    , Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS
            };

    private boolean isRegisterClicked=false,isLoginClicked=false;
    private static final int RC_SMS_RECIVE_PHONE_PERM = 123;
    private Master_data data;

    public static final String NOTIFICATION_CHANNEL_ID = "sms_service";
    public static final int NOTIFICATION_ID = 001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkmaster);
        // Set up the login form.
        myshare = getSharedPreferences("LIS", MODE_PRIVATE);
        editor = myshare.edit();

        editor.putBoolean("IsSendOTP", false);
        editor.apply();
        editor.commit();


        data = new Master_data();
        mMobileNo = (EditText) findViewById(R.id.mobile);
        mUsertype = (MaterialSpinner) findViewById(R.id.usertype);
        mLogin = (Button) findViewById(R.id.login_button);

        mUsertype.setBackgroundResource(R.drawable.background_spinner);

        usertypelist = new ArrayList<>();
        usertypelist.add("Select");
        usertypelist.add("Jodidar");
        usertypelist.add("Retailer");
        usertypelist.add("MSR");
        usertypelist.add("LISSO");
        usertypelist.add("DLR");
        usertypelist.add("Stockist");
        usertypelist.add("End User");
        mUsertype.setItems(usertypelist);


        mLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMobileNo.getText().length() != 10)
                    toasty.showFailureToast("Enter vaild mobile number");
                else if(mSelectedUsertype.equals("Select"))
                    toasty.showFailureToast("Select User type");
                else {
                    isRegisterClicked=true;
                    getPermission();
                    /*if (mSelectedUsertype.equals("Jodidar")
                            || mSelectedUsertype.equals("Retailer")
                            || mSelectedUsertype.equals("Stockist")
                            || mSelectedUsertype.equals("DLR")) {

                    } else if (mSelectedUsertype.equals("MSR") || mSelectedUsertype.equals("LISSO")) {
                        data.setMobileNo(mMobileNo.getText().toString());
                        data.setUserType(mSelectedUsertype);
                        startActivity(new Intent(CheckMaster_Activity.this, MSR_LISSO_Resgistration_Activity.class)
                                .putExtra("UserData", data));
                    } else {
                        data.setMobileNo(mMobileNo.getText().toString());
                        data.setUserType(mSelectedUsertype);
                        startActivity(new Intent(CheckMaster_Activity.this, EndUser_Activity.class)
                                .putExtra("UserData", data));
                    }*/
                }


            }
        });

        mUsertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                mSelectedUsertype = item.toString();
            }
        });


        if (hasLocationAndContactsPermissions()) {
            GetIMEI();
        }
        else
        {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.phone_state),
                    RC_SMS_RECIVE_PHONE_PERM,
                    PHONESTATE_AND_SMS);
        }

//        alertbox.newalert(getString(R.string.scan_error_login));

//        notification();

    }

    private void notification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        builder.setSmallIcon(R.drawable.notification);
        builder.setContentTitle("SMS Service");
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.scan_error_login)));
        builder.setAutoCancel(true);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat compat = NotificationManagerCompat.from(this);
        compat.notify(NOTIFICATION_ID, builder.build());
    }

    private void getPermission()
    {
        if (hasLocationAndContactsPermissions()) {
            checkInternet();
        }
        else
        {
            EasyPermissions.requestPermissions(this,
                    getString(R.string.phone_state),
                    RC_SMS_RECIVE_PHONE_PERM,
                    PHONESTATE_AND_SMS);
        }


    }

    //get the phone permission to read and sms status
    private boolean hasLocationAndContactsPermissions() {
        return EasyPermissions.hasPermissions(this, PHONESTATE_AND_SMS);
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }


    @SuppressLint("MissingPermission")
    //get the imei number to find the user change devices
    private void GetIMEI() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        mIMEI = telephonyManager.getDeviceId();
        mModelName =Build.BRAND;
        mModelNumber =Build.MODEL;
        editor.putString("IMEI",mIMEI);
        editor.putString("MODEL",mModelName);
        editor.putString("BRAND",mModelNumber);
        editor.commit();

        data.setIMEI_number(mIMEI);
        data.setModelNumber(mModelNumber);
        data.setModelName(mModelName);

        StringBuffer infoBuffer = new StringBuffer();

        infoBuffer.append("-------------------------------------\n");
        infoBuffer.append("Model :" + Build.MODEL + "\n");//The end-user-visible name for the end product.
        infoBuffer.append("Brand: " + Build.BRAND + "\n");//The consumer-visible brand with which the product/hardware will be associated, if any.
        infoBuffer.append("-------------------------------------\n");

        Log.v("Device info",infoBuffer.toString());
        if(isRegisterClicked)
        {
            checkInternet();
        }
        else if(isLoginClicked)
        {
            //goto login activity
            startActivity(new Intent(CheckMaster_Activity.this, Login_Activity.class));
        }
    }

    public void alertnew(String s) {

        Alertbox alert = new Alertbox(CheckMaster_Activity.this);
        alert.newalert(s);
    }
    //check internet
    private void checkInternet() {
        NetworkConnection connection = new NetworkConnection(this);
        if (connection.CheckInternet())
            checkMobileNumber();
        else alertbox.showAlertbox(getResources().getString(R.string.no_internet));
    }

    //check the mobile number if the user already registered in other category
    private void checkMobileNumber()  {
        try {
            loading = ProgressDialog.show(this, getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetroClient.getApiService();
            Call<Master> call = service.CheckMobile(mMobileNo.getText().toString(), mSelectedUsertype);
            call.enqueue(new Callback<Master>() {
                @Override
                public void onResponse(Call<Master> call, Response<Master> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success"))
                            OnCheckingSuccess(response.body().getData());
                        else if (response.body().getResult().equals("NotSuccess")) {
                            alertnew("Please become a "+mSelectedUsertype +" contact or visit LIS office");
//                            alertbox.showAlertbox("Please become a "+mSelectedUsertype +" contact or visit LIS office");
                        } else if (response.body().getResult().equals("Same mobile number")) {
                            alertnew(getString(R.string.same_number_found));
//                            alertbox.showAlertbox(getString(R.string.same_number_found));
                        } else if (response.body().getResult().equals("Already Register")) {
                            alertnew("You have already registered Please login");
//                            alertbox.showAlertbox("You have already registered Please login");
                        } else {
                            alertbox.showAlertbox(getString(R.string.server_error));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        alertbox.showAlertbox(getString(R.string.server_error));
                    }
                }
                @Override
                public void onFailure(Call<Master> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    alertbox.showAlertbox(getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //on success check the user type and
    private void OnCheckingSuccess(Master_data data) {

        if(data == null) {
            data = new Master_data();
            data.setUserType(mSelectedUsertype);
            data.setMobileNo(mMobileNo.getText().toString());
        }

        data.setIMEI_number(mIMEI);
        data.setModelName(mModelName);
        data.setModelNumber(mModelNumber);

        editor.putString("UserID", data.getId());
        editor.putString("UserCode", data.getDLR_code());
        editor.putString("UserType", data.getUserType());
        editor.commit();

        if (mSelectedUsertype.equals("Jodidar")
                || mSelectedUsertype.equals("Retailer")
                || mSelectedUsertype.equals("Stockist")) {
            startActivity(new Intent(CheckMaster_Activity.this, Jodidar_Registration_Activity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("UserData", data));
        }
        else if(mSelectedUsertype.equals("MSR") || mSelectedUsertype.equals("LISSO") ){
            startActivity(new Intent(CheckMaster_Activity.this, MSR_LISSO_Resgistration_Activity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("UserData", data));
        }
        else  if(mSelectedUsertype.equals("DLR") ){

            startActivity(new Intent(CheckMaster_Activity.this, DLR_Registeration_Activity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("UserData", data));
        }
        else
        {
            startActivity(new Intent(CheckMaster_Activity.this, EndUser_Activity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK)
                    .putExtra("UserData", data));
        }
    }

    public void Onlogin(View view) {


        isLoginClicked=true;
        if (hasLocationAndContactsPermissions()) {
//            checkInternet();
        startActivity(new Intent(CheckMaster_Activity.this, Login_Activity.class));
    }
        else
        {
            //permission to get phone state
            EasyPermissions.requestPermissions(this,
                    getString(R.string.phone_state),
                    RC_SMS_RECIVE_PHONE_PERM,
                    PHONESTATE_AND_SMS);
        }
    }


    //if permission granted get the imei
    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        GetIMEI();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }
}

