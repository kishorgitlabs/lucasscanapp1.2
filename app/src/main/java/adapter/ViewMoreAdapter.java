package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;

import model.orderhistory.PendingCompletedOrderList;

public class ViewMoreAdapter extends ArrayAdapter {
    List<PendingCompletedOrderList> data;
    private Context context;
    public ViewMoreAdapter(@NonNull Context context, List<PendingCompletedOrderList> data) {
        super(context, R.layout.view_more_adapter);
        this.context=context;
        this.data=data;
        data.removeAll(Collections.singletonList(null));
//        data.remove(null);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view= LayoutInflater.from(context).inflate(R.layout.view_more_adapter,null);
        TextView sNo=view.findViewById(R.id.view_more_s_no);
        TextView partNo=view.findViewById(R.id.view_more_part_number);
        TextView partName=view.findViewById(R.id.view_more_part_name);
        TextView price=view.findViewById(R.id.view_more_amount);
        TextView description = view.findViewById(R.id.view_more_description);
        TextView quantity=view.findViewById(R.id.view_more_order_quantity);
        TextView status=view.findViewById(R.id.view_more_status);
        TextView view_more_dispatchedorder_quantity=view.findViewById(R.id.view_more_dispatchedorder_quantity);

        sNo.setText(position+1+"");
        partNo.setText(data.get(position).getPartNumber());
        partName.setText(data.get(position).getPartName());
        price.setText(data.get(position).getPartPrice()+"");
        description.setText(data.get(position).getDescription());
        quantity.setText(data.get(position).getQuantity()+"");
        status.setText(data.get(position).getOrderStatus());
        if(data.get(position).getmDispatchedqty()!=null)
            view_more_dispatchedorder_quantity.setText(data.get(position).getmDispatchedqty());
        else
            view_more_dispatchedorder_quantity.setText("0");

        return view;

    }

    @Override
    public int getCount() {
        return data.size();
    }
}
