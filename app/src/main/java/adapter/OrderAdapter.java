package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.R;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.ArrayList;
import java.util.List;

import LocalDB.PartDetailsDB;
import model.Order_Parts;
import model.Order_Parts;

public class OrderAdapter extends ArrayAdapter {
    private ArrayList<Order_Parts> list;
    private Context context;

    public OrderAdapter(@NonNull Context context,ArrayList<Order_Parts> list) {
        super(context, R.layout.order_adapter);
        this.context=context;
        this.list=list;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if(convertView==null)
        {
            try {
                convertView = LayoutInflater.from(context).inflate(R.layout.order_adapter, null);
                TextView sno = convertView.findViewById(R.id.order_adapter_sno);
                TextView partNumber = convertView.findViewById(R.id.order_adapter_part_number);
                TextView partDescription = convertView.findViewById(R.id.order_adapter_part_description);
                TextView quantity = convertView.findViewById(R.id.order_adapter_quantity);
                ImageView edit = convertView.findViewById(R.id.order_adapter_edit);
                ImageView delete = convertView.findViewById(R.id.order_adapter_delete);

                sno.setText(position + 1 + "");
                partNumber.setText(list.get(position).getPartNumber());
                partDescription.setText(list.get(position).getPartDescription());
                quantity.setText(list.get(position).getPartQuantity());

                edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showAlertBox(list.get(position).getPartNumber(), list.get(position).getPartQuantity());
                    }
                });

                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showDeleteAlertBox(position);
                    }
                });

            }
            catch (OutOfMemoryError e)
            {
                e.printStackTrace();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return  convertView;
    }

    public void showAlertBox(final String partNum, String qty)
    {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        View dialogView=LayoutInflater.from(context).inflate(R.layout.cart_quantity_alert,null);
        final TextView partNumber=dialogView.findViewById(R.id.partnum_details);
        final EditText editText=dialogView.findViewById(R.id.quantity_edit_text);
        TextView save=dialogView.findViewById(R.id.cart_alert_save);
        TextView cancel=dialogView.findViewById(R.id.cart_alert_cancel);
        editText.setText(qty);
        partNumber.setText(partNum);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String qty=editText.getText().toString();
                if(qty.equals("0") || TextUtils.isEmpty(qty)) {
                    alertDialog.dismiss();
                    StyleableToast st = new StyleableToast(context,
                            "Please enter a valid number", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
                else
                {
                    PartDetailsDB partDetailsDB = new PartDetailsDB(context);
                    partDetailsDB.updateCartQuantity(partNum, qty);
                    list = partDetailsDB.getCartItem();
                    notifyDataSetChanged();
                    alertDialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.setView(dialogView);
        alertDialog.show();
    }

    private void showDeleteAlertBox(final int position)
    {
        final android.support.v7.app.AlertDialog alertDialog=new android.support.v7.app.AlertDialog.Builder(context).create();
        View deleteAlert=LayoutInflater.from(context).inflate(R.layout.cart_delete_alert,null);
        TextView yes=deleteAlert.findViewById(R.id.delete_yes);
        TextView no=deleteAlert.findViewById(R.id.delete_no);


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PartDetailsDB partDetailsDB=new PartDetailsDB(context);
                boolean bool=partDetailsDB.deleteCartItems(list.get(position).getPartNumber());
                list=partDetailsDB.getCartItem();
                notifyDataSetChanged();
                alertDialog.dismiss();

                if(!bool)
                {
                    StyleableToast st = new StyleableToast(context,
                            "Please try again", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setView(deleteAlert);
        alertDialog.show();

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }
}
