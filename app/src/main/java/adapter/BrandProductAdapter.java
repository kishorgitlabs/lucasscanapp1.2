package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.BrandSpecification;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

public class BrandProductAdapter extends ArrayAdapter {
    Context context;
    private List<String> productbrandlist;
    private String brandclicked;

    public BrandProductAdapter(@NonNull Context context, List<String> productbrandlist, String brandpost) {
        super(context, R.layout.brandproductadapter);
        this.context=context;
        this.brandclicked=brandpost;
        this.productbrandlist=productbrandlist;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if (convertView==null){

            convertView= LayoutInflater.from(context).inflate(R.layout.brandproductadapter,null);
            TextView brandproductname=convertView.findViewById(R.id.brandproductname);
            final RelativeLayout productnamelayout=convertView.findViewById(R.id.productnamelayout);

            brandproductname.setText(productbrandlist.get(position));

            productnamelayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent productspec=new Intent(context, BrandSpecification.class);
                    productspec.putExtra("productname",productbrandlist.get(position));
                    productspec.putExtra("brandclicked",brandclicked);
                    context.startActivity(productspec);
                }
            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return productbrandlist.size();
    }
}
