package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import model.ProductListAdapterModel;

import static api.retrofit.RetroClient.ROOT_URL;

public class ProductSearchAdapter extends ArrayAdapter<ProductListAdapterModel> {

    private Context context;
    private ListFilter listFilter = new ListFilter();
    private List<ProductListAdapterModel> dataListAllItems;
    private ArrayList<String> imagesList=null;
    public static List<ProductListAdapterModel> productModelList;

    public ProductSearchAdapter(@NonNull Context context, List<ProductListAdapterModel> productModel) {
        super(context, R.layout.adapter_product_search,productModel);
        this.context=context;
        this.productModelList=productModel;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view= LayoutInflater.from(context).inflate(R.layout.adapter_product_search,null);
        ImageView image=view.findViewById(R.id.image_product_id);
        TextView productName=view.findViewById(R.id.product_id);
        RelativeLayout layout=view.findViewById(R.id.layout);

        productName.setText(productModelList.get(position).productName);
        String imageUri=ROOT_URL+"ImageUpload/Product/"+productModelList.get(position).productImage;
        Picasso.with(context).load(imageUri).error(R.drawable.lucaslogopart).into(image);
        return view;
    }
    @Override
    public int getCount() {
        return productModelList.size();
    };
    public void setProductList(List<ProductListAdapterModel> productList)
    {
        this.productModelList=productList;
    }
    @NonNull
    @Override
    public Filter getFilter() {
        return listFilter;
    }
    public class ListFilter extends Filter {
        private Object lock = new Object();
        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();
            if (dataListAllItems == null) {
                synchronized (lock) {
                    dataListAllItems = new ArrayList<>(productModelList);
                }
            }
            if (prefix == null || prefix.length() == 0) {
                synchronized (lock) {
                    results.values = dataListAllItems;
                    results.count = dataListAllItems.size();
                }
            } else

           /* if (prefix == null || prefix.length() == 0) {
                matchValues.addAll(productModelList);
            }else*/
                {
                    ArrayList<ProductListAdapterModel> matchValues = new ArrayList<ProductListAdapterModel>();
                final String searchStrLowerCase = prefix.toString().toLowerCase();

//                for (String dataItem : dataListAllItems)
                for(int i=0;i<productModelList.size();i++){
//                    if (dataItem.toLowerCase().startsWith(searchStrLowerCase))
                    if(productModelList.get(i).getProductName().toLowerCase().contains(searchStrLowerCase)){
                        matchValues.add(productModelList.get(i));
                    }
                }

                results.values = matchValues;
                results.count = matchValues.size();
            }
            return results;
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((ProductListAdapterModel) resultValue).getProductName() ;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            ArrayList<ProductListAdapterModel> c = (ArrayList<ProductListAdapterModel>) results.values;
            if (results != null) {
                clear();
                for (ProductListAdapterModel cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
            else{
                clear();
                notifyDataSetChanged();
            }
            notifyDataSetChanged();
        }
    }
}
