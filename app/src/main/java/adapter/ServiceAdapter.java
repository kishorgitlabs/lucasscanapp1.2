package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.ArrayList;
import java.util.List;

import model.FullUnitSearchList;
import model.PartSearch.ChildPartsList;

public class ServiceAdapter extends ArrayAdapter {
    private Context context;
    private List<FullUnitSearchList> partList;
    private List<ChildPartsList> child;
    public ServiceAdapter(@NonNull Context context,List<ChildPartsList> child) {
        super(context, R.layout.activity_service_adapter);
        this.context=context;
        this.child=child;
//        this.partList=partList;
    }


    @Override
    public int getCount() {
        return child.size();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        final TextView ed_sino1, ed_descrip, ed_nooff, ed_partno, ed_view;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.activity_service_adapter,null);

            ed_sino1 = (TextView) convertView.findViewById(R.id.sino_service);
            ed_descrip = (TextView) convertView.findViewById(R.id.descrip);

            ed_nooff = (TextView) convertView.findViewById(R.id.nooff);
            ed_partno = (TextView) convertView.findViewById(R.id.partno);
            ed_view = (TextView) convertView.findViewById(R.id.view);

                ed_sino1.setText(child.get(position).getIllno());
                ed_descrip.setText(child.get(position).getDescription());
                ed_nooff.setText(child.get(position).getMoq());
                ed_partno.setText(child.get(position).getChildparts());

                ed_partno.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String partNo = ed_partno.getText().toString();
                        context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber", partNo));
                    }
                });
        }

        return convertView;
    }


}
