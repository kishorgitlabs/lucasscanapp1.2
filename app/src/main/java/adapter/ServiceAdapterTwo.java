package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.R;

import java.util.List;

import model.PartSearch.ServicePartsList;
import model.SearchFullUnitList;

public class ServiceAdapterTwo extends ArrayAdapter {
    private Context context;
    private List<ServicePartsList> partList;
    //get the data from activity to set in data
    public ServiceAdapterTwo(@NonNull Context context, List<ServicePartsList> partList) {
        super(context, R.layout.activity_service_adapter);
        this.context=context;
        this.partList=partList;
    }

    @Override
    public int getCount() {
        return partList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        final TextView ed_sino1, ed_descrip, ed_nooff, ed_partno, ed_view;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(context).inflate(R.layout.activity_service_adapter,null);

            ed_sino1 = (TextView) convertView.findViewById(R.id.sino_service);
            ed_descrip = (TextView) convertView.findViewById(R.id.descrip);

            ed_nooff = (TextView) convertView.findViewById(R.id.nooff);
            ed_partno = (TextView) convertView.findViewById(R.id.partno);
            ed_view = (TextView) convertView.findViewById(R.id.view);

            ed_sino1.setText(partList.get(position).getSerIlno());
            ed_descrip.setText(partList.get(position).getSerDesc());
            ed_nooff.setText(partList.get(position).getSerNoff());
            ed_partno.setText(partList.get(position).getSerChildpartno());

            ed_partno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String partNum=ed_partno.getText().toString();
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNum));
                }
            });
        }
        return convertView;
    }
}
