package adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.brainmagic.lucasindianservice.demo.AddtoCartActivity;
import com.brainmagic.lucasindianservice.demo.PartDetails;
import com.brainmagic.lucasindianservice.demo.ProductCatalogue;
import com.brainmagic.lucasindianservice.demo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import model.PartModelClass;
import model.PartNoListDetails;
import model.PartNoSearch;
import model.ProductSearchList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsAdapter extends ArrayAdapter {
    private Context context;
    private List<ProductSearchList> list;
    private TextView edpartno, ed_desc, ed_spec, tv_view, tv_sino;
    private ImageView ed_cart;
    private PartModelClass modelClass;

    public ProductDetailsAdapter(@NonNull Context context, List<ProductSearchList> list) {
        super(context, R.layout.product_detail_adapter);
        this.list=list;
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView=null;
        if(convertView==null)
        {
            convertView=LayoutInflater.from(context).inflate(R.layout.product_detail_adapter,null);
            edpartno = (TextView) convertView.findViewById(R.id.partno_product);

            ed_desc = (TextView) convertView.findViewById(R.id.product_desc);
            ed_spec = (TextView) convertView.findViewById(R.id.product_spec);
            ed_cart = (ImageView) convertView.findViewById(R.id.product_cart);
            tv_view = (TextView) convertView.findViewById(R.id.view_product);
            tv_sino = (TextView) convertView.findViewById(R.id.sino1_product);

            edpartno.setText(list.get(position).getPartNo());
            ed_desc.setText(list.get(position).getDescription());
//            ed_spec.setText(list.get(position).getPartVolt());
            ed_spec.setText("Spec");
//            ed_cart.setText(list.get(position).getPartOutputrng());
            tv_view.setText(""+list.get(position));
            tv_sino.setText(position+1+"");

            ed_spec.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String partNumber=list.get(position).getPartNo().toString();
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNumber));
                }
            });
            ed_desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String partNumber=list.get(position).getPartNo().toString();
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNumber));
                }
            });
            edpartno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String partNumber=list.get(position).getPartNo().toString();
                    context.startActivity(new Intent(context, PartDetails.class).putExtra("Partnumber",partNumber));
                }
            });

            ed_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkInternet(list.get(position).getPartNo());
                }
            });
        }
        return convertView;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    private void checkInternet(String partNumber) {
        NetworkConnection net = new NetworkConnection(context);
        if (net.CheckInternet()) {
            getpartdetails(partNumber);
        } else {
            Toast.makeText(context, "Please check your network connection and try again!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getpartdetails(final String partNumber) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(context,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);

            progressDialog.show();
            CategoryAPI service = RetroClient.getApiService();

            Call<PartNoSearch> call = service.partNoSearch(partNumber);

            call.enqueue(new Callback<PartNoSearch>() {
                @Override
                public void onResponse(Call<PartNoSearch> call, Response<PartNoSearch> response) {
                    progressDialog.dismiss();
                    if (response.body().getResult().equals("success")) {
                        List<PartNoListDetails> PartDetails = response.body().getData();
                        modelClass=new PartModelClass();
                        modelClass.setPartNo(PartDetails.get(0).getPartNo());
                        modelClass.setAppName(PartDetails.get(0).getAppname());
                        modelClass.setDescription(PartDetails.get(0).getDescription());
                        modelClass.setOemPartNo(PartDetails.get(0).getOemPartno());
                        modelClass.setProStatus(PartDetails.get(0).getProStatus());
                        modelClass.setProType(PartDetails.get(0).getProType());
                        modelClass.setMrp(PartDetails.get(0).getMrp());
                        modelClass.setProductName(PartDetails.get(0).getProductname());
                        modelClass.setPartVolt(PartDetails.get(0).getPartVolt());
                        modelClass.setPartOutputrng(PartDetails.get(0).getPartOutputrng());
                        modelClass.setProModel(PartDetails.get(0).getProModel());
                        modelClass.setProSupersed(PartDetails.get(0).getProSupersed());
                        modelClass.setPartimage(PartDetails.get(0).getmPartImage());
                        context.startActivity(new Intent(context, AddtoCartActivity.class).putExtra("addtoCart",modelClass));
                    } else {
                        final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                                context).create();
                        View dialogView=LayoutInflater.from(context).inflate(R.layout.empty_part_alert,null);
//                        View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                        alertDialog.setView(dialogView);
                        Button Ok = (Button) dialogView.findViewById(R.id.ok);
                        final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                    final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                        Message.setText("No Parts Available !");
                        Ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                context.startActivity(new Intent(context, ProductCatalogue.class));
                                alertDialog.dismiss();
                            }
                        });
//            send.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    startActivity(new Intent(PartDetails.this, SendEnquiry.class));
//                    alertDialog.dismiss();
//                }
//            });
                        alertDialog.show();
                        progressDialog.dismiss();
                        Toast.makeText(context, "No Record Found", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<PartNoSearch> call, Throwable t) {
                    final android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(
                            context).create();

//                    LayoutInflater inflater = (context).
                    View dialogView=LayoutInflater.from(context).inflate(R.layout.empty_part_alert,null);
//                    View dialogView = inflater.inflate(R.layout.empty_part_alert, null);
                    alertDialog.setView(dialogView);
                    Button Ok = (Button) dialogView.findViewById(R.id.ok);
                    final TextView Message = (TextView) dialogView.findViewById(R.id.msg);
//                final TextView send = (TextView) dialogView.findViewById(R.id.toenquiry);
                    Message.setText("No Parts Available !");
                    Ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.startActivity(new Intent(context, ProductCatalogue.class));
                            alertDialog.dismiss();
                        }
                    });
//                send.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        startActivity(new Intent(Partdetails.this, SendEnquiry.class));
//                        alertDialog.dismiss();
//                    }
//                });


                    alertDialog.show();
                    progressDialog.dismiss();
                    Toast.makeText(context, "Someting went Wrong", Toast.LENGTH_LONG).show();
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(context, "Please try again", Toast.LENGTH_LONG).show();
        }
    }
}
