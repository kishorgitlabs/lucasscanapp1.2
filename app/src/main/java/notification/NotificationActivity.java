package notification;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.brainmagic.lucasindianservice.demo.R;

public class NotificationActivity extends AppCompatActivity {

    TextView title, message;
    String Title, Message;

    @Override
    protected void onCreate(Bundle savedInsanceState){
        super.onCreate(savedInsanceState);
        setContentView(R.layout.activity_notification);
        title = findViewById(R.id.title_notif);
        message = findViewById(R.id.notif);

        Title = getIntent().getStringExtra("title");
        Message = getIntent().getStringExtra("msg");

        title.setText(Title);
        message.setText(Message);

    }
}
