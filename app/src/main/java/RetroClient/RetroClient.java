package RetroClient;

import java.util.concurrent.TimeUnit;

import ApiInterface.CategoryAPI;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient  {


    //sap quailty link
//    private static final String ROOT_URL = "http://14.142.78.188/LucasIndianServiceQuality/LucasIndianServiceQuality/api/Value/";
    // live link
    public static final String ROOT_URL = "http://14.142.78.188/LISMOB/API/Value/";
    public static final String ROOT_URLUPLOAD = "http://14.142.78.188/LISMOB/API/Upload/";
    //brainmagic link
//    private static final String ROOT_URL = "http://lucasindianjson.brainmagicllc.com/api/Value/";

    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(45, TimeUnit.MINUTES)
                .writeTimeout(45, TimeUnit.MINUTES)
                .readTimeout(45, TimeUnit.MINUTES)
                .build();
    }


    private static Retrofit getRetrofitUpload() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URLUPLOAD)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    /**
     * Get API Service
     *
     * @return API Service
     */
    public static CategoryAPI getApiService() {
        return getRetrofitInstance().create(CategoryAPI.class);
    }

    public static CategoryAPI getApiServiceUpload() {
        return getRetrofitUpload().create(CategoryAPI.class);
    }
}
