package api.models.smslog;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class SmsData {
  @SerializedName("MobileNo")
  @Expose
  private Long MobileNo;
  @SerializedName("Inserttime")
  @Expose
  private String Inserttime;
  @SerializedName("Status")
  @Expose
  private String Status;
  @SerializedName("Usertype")
  @Expose
  private String Usertype;
  @SerializedName("Sms")
  @Expose
  private String Sms;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("SendFrom")
  @Expose
  private String SendFrom;
  @SerializedName("Name")
  @Expose
  private Object Name;
  public void setMobileNo(Long MobileNo){
   this.MobileNo=MobileNo;
  }
  public Long getMobileNo(){
   return MobileNo;
  }
  public void setInserttime(String Inserttime){
   this.Inserttime=Inserttime;
  }
  public String getInserttime(){
   return Inserttime;
  }
  public void setStatus(String Status){
   this.Status=Status;
  }
  public String getStatus(){
   return Status;
  }
  public void setUsertype(String Usertype){
   this.Usertype=Usertype;
  }
  public String getUsertype(){
   return Usertype;
  }
  public void setSms(String Sms){
   this.Sms=Sms;
  }
  public String getSms(){
   return Sms;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setSendFrom(String SendFrom){
   this.SendFrom=SendFrom;
  }
  public String getSendFrom(){
   return SendFrom;
  }
  public void setName(Object Name){
   this.Name=Name;
  }
  public Object getName(){
   return Name;
  }
}