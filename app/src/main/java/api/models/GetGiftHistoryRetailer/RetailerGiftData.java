
package api.models.GetGiftHistoryRetailer;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class RetailerGiftData {

    @SerializedName("PRODUCTCODE")
    private String mPRODUCTCODE;
    @SerializedName("PRODUCTNAME")
    private String mPRODUCTNAME;
    @SerializedName("Points")
    private String mPoints;
    @SerializedName("R_DATE")
    private String mRDATE;

    public String getPRODUCTCODE() {
        return mPRODUCTCODE;
    }

    public void setPRODUCTCODE(String pRODUCTCODE) {
        mPRODUCTCODE = pRODUCTCODE;
    }

    public String getPRODUCTNAME() {
        return mPRODUCTNAME;
    }

    public void setPRODUCTNAME(String pRODUCTNAME) {
        mPRODUCTNAME = pRODUCTNAME;
    }

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

    public String getRDATE() {
        return mRDATE;
    }

    public void setRDATE(String rDATE) {
        mRDATE = rDATE;
    }

}
