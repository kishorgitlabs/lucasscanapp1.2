
package api.models.overallsearch;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class OverallSearchResult {

    @SerializedName("data")
    private List<OverallSearchData> mData;
    @SerializedName("result")
    private String mResult;

    public List<OverallSearchData> getData() {
        return mData;
    }

    public void setData(List<OverallSearchData> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
