
package api.models.overallsearch;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@SuppressWarnings("unused")
public class OverallSearchData implements Serializable {

    @SerializedName("Description")
    private String mDescription;
    @SerializedName("Part_No")
    private String mPartNo;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPartNo() {
        return mPartNo;
    }

    public void setPartNo(String partNo) {
        mPartNo = partNo;
    }

}
