package api.models.scan.validateOTP;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class ValidateOTP{
  @SerializedName("result")
  @Expose
  private String result;
  @SerializedName("data")
  @Expose
  private ValidateData data;
  public void setResult(String result){
   this.result=result;
  }
  public String getResult(){
   return result;
  }
  public void setData(ValidateData data){
   this.data=data;
  }
  public ValidateData getData(){
   return data;
  }
}