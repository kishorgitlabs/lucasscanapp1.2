
package api.models.scanhistory.detailpoints;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetRmDetailsPointsList {

    @SerializedName("date")
    private String mDate;
    @SerializedName("material")
    private String mMaterial;
    @SerializedName("modeflag")
    private String mModeflag;
    @SerializedName("points")
    private String mPoints;
    @SerializedName("UNIQUENO")
    private String mUNIQUENO;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getMaterial() {
        return mMaterial;
    }

    public void setMaterial(String material) {
        mMaterial = material;
    }

    public String getModeflag() {
        return mModeflag;
    }

    public void setModeflag(String modeflag) {
        mModeflag = modeflag;
    }

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

    public String getUNIQUENO() {
        return mUNIQUENO;
    }

    public void setUNIQUENO(String uNIQUENO) {
        mUNIQUENO = uNIQUENO;
    }

}
