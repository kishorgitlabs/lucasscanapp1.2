
package api.models.scanhistory.totalpoints;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class GetRMPointsList {

    @SerializedName("code")
    private String mCode;
    @SerializedName("points")
    private String mPoints;

    public String getCode() {
        return mCode;
    }

    public void setCode(String code) {
        mCode = code;
    }

    public String getPoints() {
        return mPoints;
    }

    public void setPoints(String points) {
        mPoints = points;
    }

}
