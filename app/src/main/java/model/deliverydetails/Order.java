package model.deliverydetails;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Order {

    @SerializedName("DLRCode")
    private String mDLRCode;
    @SerializedName("DeliveryAddress")
    private String mDeliveryAddress;
    @SerializedName("DeliveryMobile")
    private String mDeliveryMobile;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("DeliveryName")
    private String mDeliveryName;
    @SerializedName("DeliveryStatus")
    private String mDeliveryStatus;
    @SerializedName("Distributorid")
    private Long mDistributorid;
    @SerializedName("id")
    private Long mId;
    @SerializedName("OrderDate")
    private String mOrderDate;
    @SerializedName("OrderNumber")
    private Long mOrderNumber;
    @SerializedName("OrderedAddress")
    private String mOrderedAddress;
    @SerializedName("OrderedByName")
    private String mOrderedByName;
    @SerializedName("OrderedMobile")
    private String mOrderedMobile;
    @SerializedName("DistributorCode")
    private String mDistributorCode;

    @SerializedName("Distributorusertype")
    private String mDistributorusertype;
    @SerializedName("TotalOrderAmount")
    private Long mTotalOrderAmount;

    @SerializedName("Usertype")
    private String Usertype;

    public String getUsertype() {
        return Usertype;
    }

    public void setUsertype(String usertype) {
        Usertype = usertype;
    }

    @SerializedName("OrderStatus")
    private String OrderStatus;

    @SerializedName("TotalOrderedQty")
    private String TotalOrderedQty;

    @SerializedName("Active")
    private String Active;

    public String getDLRCode() {
        return mDLRCode;
    }

    public void setDLRCode(String DLRCode) {
        mDLRCode = DLRCode;
    }

    public String getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(String DeliveryAddress) {
        mDeliveryAddress = DeliveryAddress;
    }
    public String getmDistributorCode() {
        return mDistributorCode;
    }

    public void setmDistributorCode(String mDistributorCode) {
        this.mDistributorCode = mDistributorCode;
    }

    public String getmDistributorusertype() {
        return mDistributorusertype;
    }

    public void setmDistributorusertype(String mDistributorusertype) {
        this.mDistributorusertype = mDistributorusertype;
    }

    public String getDeliveryMobile() {
        return mDeliveryMobile;
    }

    public void setDeliveryMobile(String DeliveryMobile) {
        mDeliveryMobile = DeliveryMobile;
    }

    public String getDeliveryName() {
        return mDeliveryName;
    }

    public void setDeliveryName(String DeliveryName) {
        mDeliveryName = DeliveryName;
    }

    public String getDeliveryStatus() {
        return mDeliveryStatus;
    }

    public void setDeliveryStatus(String DeliveryStatus) {
        mDeliveryStatus = DeliveryStatus;
    }

    public Long getDistributorid() {
        return mDistributorid;
    }

    public void setDistributorid(Long Distributorid) {
        mDistributorid = Distributorid;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getOrderDate() {
        return mOrderDate;
    }

    public void setOrderDate(String OrderDate) {
        mOrderDate = OrderDate;
    }

    public Long getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(Long OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getOrderedAddress() {
        return mOrderedAddress;
    }

    public void setOrderedAddress(String OrderedAddress) {
        mOrderedAddress = OrderedAddress;
    }

    public String getOrderedByName() {
        return mOrderedByName;
    }

    public void setOrderedByName(String OrderedByName) {
        mOrderedByName = OrderedByName;
    }

    public String getOrderedMobile() {
        return mOrderedMobile;
    }

    public void setOrderedMobile(String OrderedMobile) {
        mOrderedMobile = OrderedMobile;
    }

    public Long getTotalOrderAmount() {
        return mTotalOrderAmount;
    }

    public void setTotalOrderAmount(Long TotalOrderAmount) {
        mTotalOrderAmount = TotalOrderAmount;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getTotalOrderedQty() {
        return TotalOrderedQty;
    }

    public void setTotalOrderedQty(String totalOrderedQty) {
        TotalOrderedQty = totalOrderedQty;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

    public String getOrderedPincode() {
        return mPincode;
    }

    public void setOrderedPincode(String OrderedPincode) {
        this.mPincode = OrderedPincode;
    }
}