
package model.PartSearch;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartSearch {

    @SerializedName("data")
    private List<PartsSearchList> mData;
    @SerializedName("result")
    private String mResult;

    public List<PartsSearchList> getData() {
        return mData;
    }

    public void setData(List<PartsSearchList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
