package orders.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.brainmagic.lucasindianservice.demo.R;

import home.Home_Activity;
import orders.QuickOrders;

public class BottomSheetNavigation extends BottomSheetDialogFragment {

    private BottomSheetListener bottomSheetListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.bottom_navigation_sheet, container,false);
        LinearLayout quickOrder=view.findViewById(R.id.quick_order_bottom_sheet);
        LinearLayout orderHistory=view.findViewById(R.id.order_history_bottom_sheet);

        quickOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    bottomSheetListener.bottomSheetListener("QuickOrder");
//                getActivity().startActivity(new Intent(getActivity(), QuickOrders.class));
            }
        });

        orderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetListener.bottomSheetListener("OrderHistory");
//                getActivity().startActivity(new Intent(getActivity(), QuickOrders.class));
            }
        });
        return view;
    }

    public interface BottomSheetListener
    {
        public void bottomSheetListener(String orderType);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            bottomSheetListener= (BottomSheetListener) context;
        }catch (Exception e)
        {

        }
    }
}
