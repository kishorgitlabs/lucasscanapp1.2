package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.SegmentAdpaterTwo;
import home.Home_Activity;
import model.OECustomers;
import model.OECustomersList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SegmentsDetailsTwo extends AppCompatActivity {
    private GridView segmentList;

    private String getValueIntent;
    private List<OECustomersList> oecustlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segments_details_two);
        ImageView back=findViewById(R.id.segment_detail_two_back);
        ImageView home=findViewById(R.id.segment_detail_two_home);
        ImageView cart=findViewById(R.id.segment_two_cart_details);
        TextView segment=findViewById(R.id.segment_details);
        TextView heading=findViewById(R.id.application);

        getValueIntent=getIntent().getStringExtra("segment");
//        ViewCompat.setTransitionName(segment, "segment");
        heading.setText(getValueIntent);
        segment.setText(getValueIntent);

        segmentList=findViewById(R.id.segment_detail_list);


//        SegmentAdpaterTwo segmentAdapter=new SegmentAdpaterTwo(getApplicationContext(),numbers);
//        segmentList.setAdapter(segmentAdapter);
        //get the make list
        getMake();
        segmentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(SegmentsDetailsTwo.this,SegmentAdapterThree.class)
                        .putExtra("make",oecustlist.get(i).getOemCustomer())
                        .putExtra("segment",getValueIntent));
            }
        });

        //go back to previous activity
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SegmentsDetailsTwo.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SegmentsDetailsTwo.this,OrderActivity.class));
            }
        });
    }

    //get the make list
    private void getMake()
    {
        NetworkConnection net=new NetworkConnection(getApplicationContext());
        if(net.CheckInternet())
        {
            final ProgressDialog progressDialog = new ProgressDialog(SegmentsDetailsTwo.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            try {
                CategoryAPI service = RetroClient.getApiService();
                Call<OECustomers> call = service.oeCustomerList(getValueIntent);
                call.enqueue(new Callback<OECustomers>() {
                    @Override
                    public void onResponse(Call<OECustomers> call, Response<OECustomers> response) {
                        if (response.body().getResult().equals("success")) {
                            oecustlist = response.body().getData();
                            segmentList.setAdapter(new SegmentAdpaterTwo(SegmentsDetailsTwo.this, oecustlist));
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }
                    @Override
                    public void onFailure(Call<OECustomers> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
