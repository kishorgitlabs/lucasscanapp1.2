package com.brainmagic.lucasindianservice.demo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ApiInterface.CategoryAPI;
import RetroClient.RetroClient;
import adapter.SegmentAdpaterTwo;
import home.Home_Activity;
import kotlin.collections.MapsKt;
import model.ModelItemList;
import model.ModelList;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SegmentAdapterThree extends AppCompatActivity {

    private GridView segmentList;
    private String Make,Segment;
    private List<ModelItemList> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segment_adapter_three);

        ImageView back=findViewById(R.id.segment_detail_three_back);
        ImageView home=findViewById(R.id.segment_detail_three_home);
        TextView makes=findViewById(R.id.make);
        ImageView cart=findViewById(R.id.segment_three_cart_details);
        TextView segment=findViewById(R.id.make_application);
        TextView makeHead=findViewById(R.id.make_details);


        segmentList=findViewById(R.id.segment_detail_list);
        Make=getIntent().getStringExtra("make");
        Segment=getIntent().getStringExtra("segment");
        segment.setText(Segment);
        makes.setText(Make);
        makeHead.setText(Make);

        //get clicked segment position to pass in json using put extra to nxt activity
        segmentList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(SegmentAdapterThree.this,ApplicationTableActivity.class)
                        .putExtra("make",Make)
                        .putExtra("segment",Segment)
                        .putExtra("model",list.get(i).getModel())

                );
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SegmentAdapterThree.this, Home_Activity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SegmentAdapterThree.this,OrderActivity.class));
            }
        });
        getModel();

    }

    private void getModel()
    {
        NetworkConnection net=new NetworkConnection(getApplicationContext());
        if(net.CheckInternet())
        {
            final ProgressDialog progressDialog = new ProgressDialog(SegmentAdapterThree.this,
                    R.style.Progress);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            try {
                CategoryAPI service = RetroClient.getApiService();
                Call<ModelList> call = service.modelList(Segment, Make);
                call.enqueue(new Callback<ModelList>() {
                    @Override
                    public void onResponse(Call<ModelList> call, Response<ModelList> response) {
                        if (response.body().getResult().equals("success")) {
                            list = response.body().getData();
                            progressDialog.dismiss();
                            SegmentAdpaterTwo segmentAdapter = new SegmentAdpaterTwo(getApplicationContext(), list, "two");
                            segmentList.setAdapter(segmentAdapter);
                        } else {

                            Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    }
                    @Override
                    public void onFailure(Call<ModelList> call, Throwable t) {

                        Toast.makeText(getApplicationContext(), "No Record Found", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Please check your network connection and try again!", Toast.LENGTH_LONG).show();
        }

    }
}
